#include <Wire.h>
#include <Arduino.h>
#include <ArduinoOTA.h>
#include <WiFiManager.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

#define INFLUXDB_URL    "https://influxdb.astw.de"
#define INFLUXDB_TOKEN  "_JcjkFGLmHD5vfKY7zFIzSthoGE7ukzjCXjyxbyxhbxKX6ixhs5FmWv50cDLXQYWYi5hx5qbKPwB3TLG-X2g7g=="
#define INFLUXDB_ORG    "astw"
#define INFLUXDB_BUCKET "test"

sensor_t sensor;
uint16_t sensor_ir;
uint16_t sensor_full;
uint16_t sensor_visible;
float    sensor_lux;

Adafruit_TSL2591 tsl_sensor = Adafruit_TSL2591(2591);
InfluxDBClient   influx_client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);
Point            influx_point("sqm");

void displaySensorDetails(void){
    Serial.println(F("------------------------------------"));
    Serial.print  (F("Sensor:       ")); Serial.println(sensor.name);
    Serial.print  (F("Driver Ver:   ")); Serial.println(sensor.version);
    Serial.print  (F("Unique ID:    ")); Serial.println(sensor.sensor_id);
    Serial.print  (F("MCU ID:       ")); Serial.println(ESP.getChipId());
    Serial.print  (F("Max Value:    ")); Serial.print  (sensor.max_value);     Serial.println(F(" lux"));
    Serial.print  (F("Min Value:    ")); Serial.print  (sensor.min_value);     Serial.println(F(" lux"));
    Serial.print  (F("Resolution:   ")); Serial.print  (sensor.resolution, 4); Serial.println(F(" lux"));
    Serial.println(F("------------------------------------"));
    Serial.println(F(""));
}

void setupWifi(){
    WiFi.mode(WIFI_STA);
    WiFiManager wm;
    wm.resetSettings();
    wm.setConfigPortalTimeout(60 * 5);
    wm.setClass("invert");
    wm.setScanDispPerc(true);

    if(!wm.autoConnect("ASTW-SQM")) {
        Serial.println("Failed to connect");
        ESP.restart();
    }else {
        Serial.println("connected");
    }

    timeSync("CET-1CEST,M3.5.0,M10.5.0/3", "time.google.com", "time.ntp.org");

    ArduinoOTA.setHostname("ASTW-SQM");
    ArduinoOTA.setPassword((const char *)"sd87gidewhvwc8i");
    ArduinoOTA.begin();
}

void setupSensor(){
    if (tsl_sensor.begin()){
        Serial.println(F("Found a TSL2591 sensor"));
    }else{
        Serial.println(F("No sensor found ... check your wiring?"));
        delay(5000);
        ESP.restart();
    }
    tsl_sensor.getSensor(&sensor);
    tsl_sensor.setTiming(TSL2591_INTEGRATIONTIME_500MS);
}

void calibrateSensor(){
    tsl2591Gain_t newtime = TSL2591_GAIN_MAX;
    if(sensor_full > 100   ) { newtime = TSL2591_GAIN_HIGH; }
    if(sensor_full > 1000  ) { newtime = TSL2591_GAIN_MED;  }
    if(sensor_full > 10000 ) { newtime = TSL2591_GAIN_LOW;  }
    tsl_sensor.setGain(newtime);
}

void measure(){
    uint32_t lum = tsl_sensor.getFullLuminosity();
    sensor_ir    = lum >> 16;
    sensor_full  = lum & 0xFFFF;
    sensor_lux   = tsl_sensor.calculateLux(sensor_full, sensor_ir);
    Serial.print(F("[ "));        Serial.print(millis());                Serial.print(F(" ms ] "));
    Serial.print(F("IR: "));      Serial.print(sensor_ir);               Serial.print(F("  "));
    Serial.print(F("Full: "));    Serial.print(sensor_full);             Serial.print(F("  "));
    Serial.print(F("Visible: ")); Serial.print(sensor_full - sensor_ir); Serial.print(F("  "));
    Serial.print(F("Lux: "));     Serial.println(sensor_lux, 6);
}

void setupInflux(){
    influx_client.setInsecure();
    influx_point.addTag("mcu_id",    String(ESP.getChipId()));
    influx_point.addTag("sensor_id", String(sensor.sensor_id));
}

void setup(){
    Serial.begin(9600);
    Serial.println("=====================");
    setupSensor();
    setupWifi();
    setupInflux();
    displaySensorDetails();
    Serial.println("=====================");
    measure();
}

void sendData(){
    influx_point.clearFields();
    influx_point.addField("rssi",    WiFi.RSSI()    );
    influx_point.addField("uptime",  millis()       );
    influx_point.addField("ir",      sensor_ir      );
    influx_point.addField("full",    sensor_full    );
    influx_point.addField("visible", sensor_visible );
    influx_point.addField("lux",     sensor_lux     );
    influx_point.addField("timing",  (tsl_sensor.getTiming() + 1) * 100);
    influx_point.addField("gain",    tsl_sensor.getGain());

    Serial.println(influx_client.pointToLineProtocol(influx_point));

    if (!influx_client.writePoint(influx_point)) {
        Serial.print("InfluxDB write failed: ");
        Serial.println(influx_client.getLastErrorMessage());
        delay(1000);
        ESP.restart();
    }
}

void loop(){
    ArduinoOTA.handle();
    calibrateSensor();
    measure();
    sendData();
    delay(1000);
}
